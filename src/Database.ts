import _fetch from './fetch';
import Event from './Event';

class Database {

    private webService = 'https://dhbw.cheekbyte.de/calendar/4691004/';

    private createEventsFromJson(json: any) {
        let events: Event[] = [];

        json.forEach((element: any) => {
            events.push(new Event(new Date(element.start), new Date(element.end), element.title, element.organizer, element.allday, element.status, element.location, element.webpage, element.categories, element.imageurl, element.id));
        });

        return events;
    }

    public getEvents(): Promise<Event[]> {
        return _fetch(this.webService + 'events', {Method: 'GET'})
            .then(response => response.json())
            .then(json => this.createEventsFromJson(json))
    }

    public addEvent(evt: Event) {
        let method: 'PUT' | 'POST';
        let param: string;

        if (!evt.id) {
            method = 'POST';
            param = '';
        } else {
            method = 'PUT';
            param = '/' + evt.id;
        }

        let catsInEvent = evt.categories.reduce((obj: any, cat) => {
            obj[cat.name] = cat.id;
            return obj;
        }, {});

        return this.getCategories().then(allCategories => {
            let catsOnServer = allCategories.reduce((obj: any, cat) => {
                obj[cat.name] = cat.id;
                return obj;
            }, {});
            let catProm: Promise<{ id: number, name: string }>[] = [];
            for (let [name, id] of Object.entries(catsInEvent)) {
                if (id) catProm.push(Promise.resolve({id: id as number, name: name}));
                if (name in catsOnServer) catProm.push(Promise.resolve({name: name, id: catsOnServer[name]}));
                else catProm.push(this.createCategory({name: name}));
            }
            return Promise.all(catProm);
        }).then((test: { id: number, name: string }[]) => {
            evt.categories = test;

            return _fetch(`${this.webService}events${param}`, {Method: method, Body: evt})
                .then(response => response.json())
        });

    }

    public deleteEvent(evt: Event) {
        return _fetch(`${this.webService}events/${evt.id}`, {Method: 'DELETE'});
    }

    public getCategories(): Promise<{ id?: number, name: string }[]> {
        return _fetch(`${this.webService}categories`, {Method: 'GET'})
            .then(response => response.json())
    }

    public createCategory(category: object): Promise<{ id: number, name: string }> {
        return _fetch(`${this.webService}categories`, {Method: 'POST', Body: category})
            .then(response => response.json())
    }
}

export default Database;
