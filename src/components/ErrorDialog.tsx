import * as React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";

interface Props {
    content: string;
    onClose: () => void;
    open: boolean;
}

class ErrorDialog extends React.Component<Props, object> {
    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | undefined {
        return <Dialog
            open={this.props.open}
            onClose={this.props.onClose}>
            <DialogTitle>An error occurred</DialogTitle>
            <DialogContent>
                <DialogContentText>{this.props.content}</DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' onClick={this.props.onClose}>Dismiss</Button>
            </DialogActions>
        </Dialog>;
    }
}

export default ErrorDialog;
