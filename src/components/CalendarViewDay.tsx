import * as React from 'react';
import {createStyles, Grid, Theme, WithStyles} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import DayColumn from './DayColumn';
import TimeLabelDisplay from './TimeLabelDisplay';
import Event from '../Event';

const styles = (theme: Theme) => createStyles({
    h100: {
        height: 'calc(100% - 20px)',
        marginTop: 10,
        marginBottom: 10,
    }
});

interface Props extends WithStyles<typeof styles> {
    refDate: Date;
    events: Event[];
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
}

interface State {
}

class CalendarViewDay extends React.Component<Props, State> {
    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        let {classes, refDate} = this.props;

        let rFunc: ((ref: HTMLDivElement) => void) | undefined = undefined;
        let cProm = new Promise<HTMLDivElement>(resolve => rFunc = (ref: HTMLDivElement) => {
            resolve(ref);
        });

        return <React.Fragment>
            <TimeLabelDisplay alignPromise={cProm}/>
            <Grid container direction='row' className={classes.h100}
                  justify='center' alignItems='stretch'>
                <Grid item xs={12} sm={8} md={4}>
                    <DayColumn primary={true} date={refDate} contentRef={rFunc} events={this.props.events}
                        editEvent={this.props.editEvent} deleteEvent={this.props.deleteEvent}/>
                </Grid>
            </Grid>
        </React.Fragment>;
    }
}

export default withStyles(styles)(CalendarViewDay);
