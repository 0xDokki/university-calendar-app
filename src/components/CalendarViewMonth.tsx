import * as React from 'react';
import {Box, createStyles, Hidden, Theme, Typography, WithStyles} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import DayColumn from './DayColumn';
import classNames from 'classnames';
import {range} from 'lodash';
import Event from '../Event';

const viewStyles = (theme: Theme) => createStyles({
    gridMaster: {
        gridTemplateColumns: 'repeat(5, 2fr) repeat(2, 1fr)',
        gridGap: 10,
        margin: 10,
        height: 'calc(100% - 20px)',
        display: 'grid',
        justifyItems: 'stretch',
        alignItems: 'stretch',
        minHeight: 720,
    },
    weeks5: {
        gridTemplateRows: '1.43em repeat(5, 1fr)',
    },
    weeks6: {
        gridTemplateRows: '1.43em repeat(6, 1fr)',
    },
});

interface ViewProps extends WithStyles<typeof viewStyles> {
    refDate: Date;
    events: Event[];
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
}

interface ViewState {
}

interface GridItemStyle {
    gridColumn: number | string;
    gridRow: number | string;
    minWidth: 0;
}

class CalendarViewMonth extends React.Component<ViewProps, ViewState> {
    private static findOffsetToNearestMonday(startDate: Date): number {
        let date = new Date(startDate.getFullYear(), startDate.getMonth(), 0);
        let o = 0;
        while (date.getDay() !== 1) {
            date.setDate(date.getDate() - 1);
            o--;
        }
        if (o > -6) return o;

        date = new Date(startDate.getFullYear(), startDate.getMonth(), 0);
        o = 0;
        while (date.getDay() !== 1) {
            date.setDate(date.getDate() + 1);
            o++;
        }
        return o;
    }

    private static deriveGridStyle(x: number, y: number): GridItemStyle {
        return {
            gridColumn: x + 1,
            gridRow: y + 1,
            minWidth: 0,
        };
    }

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        let {classes, refDate} = this.props;

        let year = refDate.getFullYear();
        let month = refDate.getMonth();
        let offset = CalendarViewMonth.findOffsetToNearestMonday(refDate);
        let wn = range(5);

        // when there's a long month with a big offset, we need to display 6 weeks (e.g. december 2018)
        let ao = Math.abs(offset) - 3;
        let isLongMonth = ao > 0 && ((new Date(year, month + 1, 0).getDate()) + ao) > 31;

        if (isLongMonth)
            wn.push(5);

        let weeks = wn.map((w, y) => {
            let o = (offset) + (w * 7);
            let days = range(7).map((d, x) => {
                let date = new Date(year, month, o + d);
                return <Box key={x} style={CalendarViewMonth.deriveGridStyle(x, y + 1)}>
                    <DayColumn date={date} primary={date.getMonth() === month}
                               isInMonthView={true} events={this.props.events}
                               editEvent={this.props.editEvent} deleteEvent={this.props.deleteEvent}/>
                </Box>;
            });
            return <React.Fragment key={y}>
                {days}
            </React.Fragment>
        });

        let descriptors = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'].map((d, i) => {
            let smallText = d.substr(0, 1);
            return <Box key={i} style={CalendarViewMonth.deriveGridStyle(i, 0)} textAlign='center'>
                <Hidden mdUp><Typography>{smallText}</Typography></Hidden>
                <Hidden smDown><Typography>{d}</Typography></Hidden>
            </Box>
        });

        return <Box className={classNames(classes.gridMaster, isLongMonth ? classes.weeks6 : classes.weeks5)}>
            {descriptors}
            {weeks}
        </Box>;
    }
}

export default withStyles(viewStyles)(CalendarViewMonth);
