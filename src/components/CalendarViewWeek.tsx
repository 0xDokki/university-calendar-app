import * as React from 'react';
import {createStyles, Grid, Hidden, Theme, Typography, WithStyles} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import DayColumn from './DayColumn';
import {range} from 'lodash';
import TimeLabelDisplay from './TimeLabelDisplay';
import Event from '../Event';

const styles = (theme: Theme) => createStyles({
    root: {
        padding: 10,
        paddingLeft: 70, // this padding is for the labels
        display: 'flex',
    },
    h100: {
        height: '100%',
    },
    descriptor: {
        textAlign: 'center',
    },
    dayColumns: {
        flexBasis: 0,
        flexGrow: 1,
    },
    column: {
        padding: 5,
    }
});

interface Props extends WithStyles<typeof styles> {
    refDate: Date;
    events: Event[];
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
}

interface State {
}

class CalendarViewWeek extends React.Component<Props, State> {
    private static findOffsetToNearestMonday(startDate: Date): number {
        let date = new Date(startDate.getTime());
        let o = 0;
        while (date.getDay() !== 1) {
            date.setDate(date.getDate() - 1);
            o--;
        }
        if (o > -6) return o;

        date = new Date(startDate.getTime());
        o = 0;
        while (date.getDay() !== 1) {
            date.setDate(date.getDate() + 1);
            o++;
        }
        return o;
    }

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        const {classes, refDate} = this.props;

        let y = refDate.getFullYear();
        let m = refDate.getMonth();
        let offset = CalendarViewWeek.findOffsetToNearestMonday(refDate);

        let cProm: Promise<HTMLDivElement>;

        let days = range(7).map(d => {
            let size: 1 | 2 = ((d < 5) ? 2 : 1);

            let date = new Date(y, m, refDate.getDate() + offset + d);

            let rFunc: ((ref: HTMLDivElement) => void) | undefined = undefined;
            if (d === 0) {
                cProm = new Promise(resolve => rFunc = (ref: HTMLDivElement) => {
                    resolve(ref);
                });
            }

            return <Grid key={offset + d} item xs={size} className={classNames(classes.h100, classes.column)}>
                <DayColumn date={date} primary={size === 2} contentRef={rFunc} events={this.props.events}
                           editEvent={this.props.editEvent} deleteEvent={this.props.deleteEvent}/>
            </Grid>;
        });

        let descriptors = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'].map(d => {
            let size: 1 | 2 = d !== 'Saturday' && d !== 'Sunday' ? 2 : 1;
            let smallText = d.substr(0, 1);
            return <Grid key={d} item xs={size}>
                <Hidden mdUp><Typography>{smallText}</Typography></Hidden>
                <Hidden smDown><Typography>{d}</Typography></Hidden>
            </Grid>
        });

        // @ts-ignore this is for the alignPromise, which is assigned, but not directly enough.
        return <React.Fragment><TimeLabelDisplay alignPromise={cProm}/>
            <Grid container direction='column' justify='space-evenly'
                  alignItems='flex-start' className={classNames(classes.root, classes.h100)}>
                <Grid item container direction='row' className={classes.descriptor}
                      justify='space-evenly' alignItems='flex-start'>
                    {descriptors}
                </Grid>
                <Grid item container direction='row' className={classes.dayColumns}
                      justify='space-evenly' alignItems='flex-start'>
                    {days}
                </Grid>
            </Grid>
        </React.Fragment>;
    }
}

export default withStyles(styles)(CalendarViewWeek);
