import * as React from 'react';
import {
    Box, Button,
    Card, CardActions,
    CardHeader,
    CardMedia, ClickAwayListener,
    createStyles, IconButton, RootRef,
    Theme,
    Typography,
    withStyles,
    WithStyles, withTheme,
} from '@material-ui/core';
import {DeleteForever, Edit} from '@material-ui/icons'
import Event from '../Event';
import CardContent from '@material-ui/core/CardContent';
import {format} from 'date-fns';

const styles = (theme: Theme) => createStyles({
    root: {
        minWidth: 300,
        maxWidth: 350,
    },
    header: {
        backgroundColor: '#212121',
    },
    image: {
        height: 0,
        paddingTop: '56.25%',
    },
    iconButton: {
        marginLeft: 'auto',
    },
    value: {
        paddingLeft: 15,
    },
});

interface Props extends WithStyles<typeof styles> {
    open: boolean;
    x: number;
    y: number;
    onClose: () => void;
    event?: Event;
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
}

interface State {
}

class DetailsPopover extends React.Component<Props, State> {
    private fixPosition = (ref: HTMLDivElement) => {
        if (!ref) return;

        let ol = ref.clientWidth;
        let lPos = this.props.x;
        if (document.body.clientWidth - lPos < ol) lPos = document.body.clientWidth - ol;
        ref.style.left = `${lPos + window.pageXOffset}px`;

        let ot = ref.clientHeight;
        let tPos = this.props.y;
        if (document.body.clientHeight - tPos < ot) tPos = document.body.clientHeight - ot;
        ref.style.top = `${tPos + window.pageYOffset}px`;
    };

    private editEvent = () => {
        if (!this.props.event) return;
        this.props.onClose();
        this.props.editEvent(this.props.event);
    };

    private deleteEvent = () => {
        if (!this.props.event) return;
        this.props.onClose();
        this.props.deleteEvent(this.props.event);
    };

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        if (!this.props.open || !this.props.event)
            return null;

        let {classes, event} = this.props;

        let media = event.imagedata ? <CardMedia image={event.imagedata} className={classes.image}/> : null;

        let website = event.webpage;
        if (website) if (!website.startsWith('http')) website = 'https://' + website;

        let webPage = website ? <Button href={website} target='event_website'>Website</Button> : null;

        let categories = event.categories.length > 0 ? (<React.Fragment>
            <Typography variant='caption'>Categories</Typography>
            <Typography className={classes.value}>{event.categories.map(cat => cat.name).join(', ')}</Typography>
        </React.Fragment>) : null;

        return <ClickAwayListener onClickAway={this.props.onClose}>
            <RootRef rootRef={this.fixPosition}>
                <Box position='absolute' zIndex={9001}>
                    <Card className={classes.root} elevation={16}>
                        <CardHeader className={classes.header} title={event.title} subheader={event.location}/>
                        {media}
                        <CardContent>
                            <Typography variant='caption'>Organizer</Typography>
                            <Typography className={classes.value}>{event.organizer}</Typography>
                            <Typography variant='caption'>Start date</Typography>
                            <Typography className={classes.value}>{format(event.start, 'do MMM yy, H:mm')}</Typography>
                            <Typography variant='caption'>End date</Typography>
                            <Typography className={classes.value}>{format(event.end, 'do MMM yy, H:mm')}</Typography>
                            <Typography variant='caption'>Status</Typography>
                            <Typography className={classes.value}>{event.status}</Typography>
                            {categories}
                        </CardContent>
                        <CardActions className={classes.header}>
                            {webPage}
                            {/* todo: add functionality to these buttons, requires DB functionality */}
                            <IconButton href='#' className={classes.iconButton}
                                        onClick={this.editEvent}><Edit/></IconButton>
                            <IconButton href='#' onClick={this.deleteEvent}><DeleteForever/></IconButton>
                        </CardActions>
                    </Card>
                </Box>
            </RootRef>
        </ClickAwayListener>;
    }
}

export default withTheme(withStyles(styles)(DetailsPopover));
