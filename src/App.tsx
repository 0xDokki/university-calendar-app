import React from 'react';
import {
    AppBar,
    Box, Button, ButtonGroup, createMuiTheme,
    createStyles,
    CssBaseline, FormControl, InputBase, MenuItem, Select,
    Theme,
    Toolbar,
    Typography,
    withStyles,
    WithStyles,
} from '@material-ui/core';
import {CalendarToday, ArrowLeft, ArrowRight, Add, Refresh} from '@material-ui/icons';
import CalendarViewMonth from './components/CalendarViewMonth';
import CalendarViewWeek from './components/CalendarViewWeek';
import CalendarViewDay from './components/CalendarViewDay';
import CalendarViewEventList from './components/CalendarViewEventList';
import {DatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import {indigo, teal} from '@material-ui/core/colors';
import {MuiThemeProvider} from '@material-ui/core/styles';
import createPalette from '@material-ui/core/styles/createPalette';
import EventDialog from './components/EventDialog';
import Database from './Database';
import Event from './Event';
import IconButton from '@material-ui/core/IconButton';
import ErrorDialog from './components/ErrorDialog';

const styles = (theme: Theme) => createStyles({
    vbox: {
        height: '100vh',
    },
    hbox: {
        width: '100vw',
    },
    white: {
        color: '#fff',
    },
    main: {
        height: '100%',
    },
    icon: {
        marginRight: theme.spacing(2),
    },
    scopeText: {
        margin: theme.spacing(0, 2, 0, 2),
        width: 150,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    footer: {
        backgroundColor: theme.palette.primary.main,
        padding: theme.spacing(2),
    },
});

const StyledInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            'label + &': {
                marginTop: theme.spacing(3),
            },
        },
        input: {
            borderRadius: 4,
            position: 'relative',
            backgroundColor: theme.palette.primary.main,
            border: '1px solid #ced4da',
            fontSize: 16,
            width: 'auto',
            padding: '8px 26px 8px 12px',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            // Use the system font instead of the default Roboto font.
            fontFamily: [
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            '&:focus': {
                borderRadius: 4,
                borderColor: '#80bdff',
                backgroundColor: theme.palette.primary.main,
                boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
        },
    }),
)(InputBase);

const titleTheme = createMuiTheme({
    palette: createPalette({
        type: 'dark',
        primary: indigo,
        secondary: teal,
        background: {
            default: '#171717',
        },
    }),
    overrides: {
        MuiInputBase: {
            root: props => ({color: 'white'}),
        },
    },
});

interface State {
    currentScope: 'day' | 'week' | 'month' | 'eventList';
    refDate: Date;
    events: Event[];
    showCreateDialog: boolean;
    currentlyEditedEvent?: Event;
    errorDialogOpen: boolean;
    errorDialogMessage: string;
}

interface Props extends WithStyles<typeof styles> {
}


class App extends React.Component<Props, State> {

    private db = new Database();

    constructor(props: Props) {
        super(props);

        this.state = {
            currentScope: 'month',
            refDate: new Date(),
            events: [],
            showCreateDialog: false,
            errorDialogOpen: false,
            errorDialogMessage: '',
        }
    }

    public componentDidMount() {
        this.db.getEvents().then(events => this.setState({events: events}))
            .catch(this.onError('Error while fetching events.'));
    }

    private handleChange = (name: string) => (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
        this.setState({
            ...this.state,
            [name]: event.target.value,
        });
    };

    private handleDateChange = (date: Date | null) => {
        this.setState({refDate: date || new Date()});
    };

    private scopeNav = (dir: number) => () => {
        let ref = this.state.refDate;
        if (this.state.currentScope === 'month') {
            ref.setMonth(ref.getMonth() + dir);
            ref.setDate(1);
        } else if (this.state.currentScope === 'week')
            ref.setDate(ref.getDate() + (7 * dir));
        else if (this.state.currentScope === 'day')
            ref.setDate(ref.getDate() + dir);
        this.setState({refDate: ref});
    };

    private setScopeToCurrent = () => {
        this.setState({refDate: new Date()});
    };

    private toggleDialog = () => {
        this.setState({showCreateDialog: !this.state.showCreateDialog, currentlyEditedEvent: undefined});
    };

    private onAddEvent = (evt: Event) => {
        this.db.addEvent(evt)
            .then(() => this.db.getEvents().then(events => this.setState({events: events})))
            .catch(this.onError('Error while adding event.'));
    };

    private refreshEvents = () => {
        this.db.getEvents().then(events => this.setState({events: events}))
            .catch(this.onError('Error while fetching events.'));
    };

    private editEvent = (evt: Event) => {
        this.setState({showCreateDialog: true, currentlyEditedEvent: evt});
    };

    private deleteEvent = (evt: Event) => {
        this.db.deleteEvent(evt).then(this.refreshEvents)
            .catch(this.onError('Error while deleting event.'));
    };

    private onError = (msg: string) => () => {
        this.setState({
            errorDialogOpen: true,
            errorDialogMessage: msg + '\nPlease try again later.'
        })
    };

    private closeErrorDialog = () => {
        this.setState({errorDialogOpen: false});
    };

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> {
        let {classes} = this.props;
        let ref = this.state.refDate;

        let module: React.ReactElement | null = null;
        if (this.state.currentScope === 'month')
            module = <CalendarViewMonth refDate={ref} events={this.state.events}
                                        editEvent={this.editEvent} deleteEvent={this.deleteEvent}/>;
        else if (this.state.currentScope === 'week')
            module = <CalendarViewWeek refDate={ref} events={this.state.events}
                                       editEvent={this.editEvent} deleteEvent={this.deleteEvent}/>;
        else if (this.state.currentScope === 'day')
            module = <CalendarViewDay refDate={ref} events={this.state.events}
                                      editEvent={this.editEvent} deleteEvent={this.deleteEvent}/>;
        else if (this.state.currentScope === 'eventList')
            module = <CalendarViewEventList events={this.state.events}
                                            editEvent={this.editEvent} deleteEvent={this.deleteEvent}/>;

        return (
            <MuiThemeProvider theme={titleTheme}>
                {/* Fuck off, this forces usage of some random date library just to use the picker */}
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Box display='flex' flexDirection='column' className={classes.vbox}>
                        <CssBaseline/>
                        <EventDialog open={this.state.showCreateDialog} onClose={this.toggleDialog}
                                     callback={evt => {
                                         this.onAddEvent(evt);
                                         this.toggleDialog();
                                     }} existingEvent={this.state.currentlyEditedEvent}/>
                        <ErrorDialog open={this.state.errorDialogOpen} content={this.state.errorDialogMessage}
                                     onClose={this.closeErrorDialog}/>
                        <Box>
                            <AppBar position='static' color='primary'>
                                <Toolbar>
                                    <Box display='flex' flexDirection='row' className={classes.hbox}>
                                        <Box display='flex' alignItems='center'>
                                            <CalendarToday className={classes.icon}/>
                                            <Typography variant='h6' color='inherit' noWrap>
                                                Fancy Calendar
                                            </Typography>
                                            <IconButton onClick={this.toggleDialog}><Add/></IconButton>
                                            <IconButton onClick={this.refreshEvents}><Refresh/></IconButton>
                                        </Box>
                                        <Box flexGrow={1}/>
                                        <Box display='flex' alignItems='center'>
                                            <DatePicker format='MMMM do yyyy'
                                                        className={classes.scopeText}
                                                        variant='inline'
                                                        value={ref}
                                                        onChange={this.handleDateChange}
                                                        views={['year', 'month', 'date']}
                                            />

                                            <ButtonGroup color='inherit'>
                                                <Button onClick={this.scopeNav(-1)}><ArrowLeft/></Button>
                                                <Button onClick={this.setScopeToCurrent}>Today</Button>
                                                <Button onClick={this.scopeNav(+1)}><ArrowRight/></Button>
                                            </ButtonGroup>

                                            <FormControl variant='outlined' className={classes.formControl}>
                                                <Select
                                                    value={this.state.currentScope}
                                                    onChange={this.handleChange('currentScope')}
                                                    input={<StyledInput name='scope' id='scope-customized-select'/>}>
                                                    <MenuItem value='day'>Day</MenuItem>
                                                    <MenuItem value='week'>Week</MenuItem>
                                                    <MenuItem value='month'>Month</MenuItem>
                                                    <MenuItem value='eventList'>Event List</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Box>
                                    </Box>
                                </Toolbar>
                            </AppBar>
                        </Box>
                        <Box flexGrow={1}>
                            <main className={classes.main}>
                                {module}
                            </main>
                        </Box>
                        <footer className={classes.footer}>
                            <Typography variant='h6' align='center' gutterBottom>
                                Footer
                            </Typography>
                            <Typography variant='subtitle1' align='center' color='textSecondary' component='p'>
                                Something here to give the footer a purpose!
                            </Typography>
                        </footer>
                    </Box>
                </MuiPickersUtilsProvider>
            </MuiThemeProvider>
        );
    }
}

export default withStyles(styles)(App);
