interface Data {
    Body?: object;
    Query?: any
    Method?: 'GET' | 'POST' | 'PUT' | 'DELETE',
}

// wrapper for node-fetch to make it easier to use
function _fetch(req: string, dat?: Data) {
    if (dat && dat.Body && dat.Query) throw new Error('Invalid parameters');
    let params = (dat && dat.Query) ? ('?' + new URLSearchParams(dat.Query)) : '';

    let opts = {
        method: ((dat && dat.Method) ? dat.Method : 'GET'),
        headers: {'Content-Type': 'application/json'},
        body: dat ? JSON.stringify(dat.Body) : undefined,
    };

    // this might still throw errors in the console for the OPTIONS request which are necessary for CORS and can't be disabled
    return fetch(req + params, opts);
}

export default _fetch;
