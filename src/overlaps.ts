import * as _ from 'lodash';
import Event from './Event';

function supersetInner<T>(src: T[]): T[][] {
    if (src.length === 0) return [];

    const withouts = _.map(src, elem => superset(_.without(src, elem)));
    return [src, ..._.flatten(withouts)];
}

function superset<T>(src: T[]): T[][] {
    return _.uniqWith(supersetInner(src), _.isEqual).sort();
}

function overlaps(e1: Event, e2: Event): boolean {
    return (e1.start >= e2.start && e1.start <= e2.end) || (e1.end >= e2.start && e1.end <= e2.end) ||
        (e2.start >= e1.start && e2.start <= e1.end) || (e2.end >= e1.start && e2.end <= e1.end);
}

interface OverlapResult {
    maxWidth: number;
    affected: {evt: Event, offset: number}[];
}

function checkOverlaps(events: Event[]): OverlapResult {
    let s = superset(events);
    let ret: OverlapResult = {
        maxWidth: 0,
        affected: [],
    };

    let res = _.maxBy(s, (evts) => {
        if (evts.length < 2 ||
            evts.slice(0, evts.length - 1).every((e1, m) => evts.slice(m + 1).every(e2 => overlaps(e1, e2)))) {
            evts.forEach((e, i) => ret.affected.push({evt: e, offset: i}));
            return evts.length;
        }
        return 0;
    });

    ret.maxWidth = res ? res.length : 0;
    ret.affected = _.uniq(ret.affected);

    return ret;
}

export default checkOverlaps;
